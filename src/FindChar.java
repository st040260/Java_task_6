import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindChar {
    public static Integer findChar(String path, String character){
        int countOfCharacter=0;
        try{
            String testFile = new String(Files.readAllBytes(Paths.get(path)));
            Pattern p = Pattern.compile(character);
            Matcher m = p.matcher(testFile);
            while(m.find()) {
                countOfCharacter++;
            }
        }catch (Exception e){}
        return  countOfCharacter;
    }
}
